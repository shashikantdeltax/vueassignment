import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ViewUI from 'view-design';
import 'view-design/dist/styles/iview.css';
import locale from 'iview/dist/locale/en-US';

Vue.config.productionTip = false
Vue.use(ViewUI, {locale: locale})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
