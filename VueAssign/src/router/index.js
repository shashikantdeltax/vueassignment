import Vue from 'vue'
import VueRouter from 'vue-router'
import MovieList from '../views/MovieList.vue'
import ActorList from '../views/ActorList.vue'
import ProducerList from '../views/ProducerList.vue'
import MovieCreate from '../views/MovieCreate.vue'
import MovieDetails from '../components/MovieDetails.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '',
    name: 'MovieList',
    component: MovieList,
    
  },
  {
    path: '/moviedetail/:id',
    component: MovieDetails,
    name: 'movieDetail'
  },
  {
    path: '/actorList',
    name: 'ActorList',
    component: ActorList
  },
  {
    path: '/producerList',
    name: 'ProducerList',
    component: ProducerList
  },
  {
    path: '/movieCreate',
    name: 'MovieCreate',
    component: MovieCreate
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    //component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
